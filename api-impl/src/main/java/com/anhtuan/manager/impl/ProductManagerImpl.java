/**
 * 
 */
package com.anhtuan.manager.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.anhtuan.HibernateDaoTemplate;
import com.anhtuan.dao.CartDAO;
import com.anhtuan.dao.CheeseDAO;
import com.anhtuan.dto.Cart;
import com.anhtuan.dto.Cheese;
import com.anhtuan.manager.ProductManager;

/**
 * @author anhtuan_truong
 * 
 */
public class ProductManagerImpl implements ProductManager {

	private CheeseDAO cheeseDAO;
	private CartDAO cartDAO;

	/**
	 * Used by Spring
	 * 
	 * @param cheeseDAO
	 */
	public void setCheeseDAO(CheeseDAO cheeseDAO) {
		this.cheeseDAO = cheeseDAO;
	}

	/**
	 * Used by Spring
	 * 
	 * @param cartDAO
	 */
	public void setCartDAO(CartDAO cartDAO) {
		this.cartDAO = cartDAO;
	}

	// Not name this method is getCheeseDAO. (It will throw exception Invalid
	// property 'cheeseDAO' of bean class
	// [com.anhtuan.manager.impl.ProductManagerImpl]: Bean property 'cheeseDAO'
	// is not writable or has an invalid setter method. Does the parameter type
	// of the setter match the return type of the getter?
	@SuppressWarnings("unchecked")
	public HibernateDaoTemplate<Cheese> getDAOCheese() {
		return ((HibernateDaoTemplate<Cheese>) cheeseDAO);
	}

	@SuppressWarnings("unchecked")
	public HibernateDaoTemplate<Cart> getDAOCart() {
		return (HibernateDaoTemplate<Cart>) cartDAO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.anhtuan.manager.CheeseManager#getCheeses(org.hibernate.criterion.
	 * DetachedCriteria)
	 */
	@Override
	public List<Cheese> getAllCheeses() {
		return getDAOCheese().findByCriteria(
				DetachedCriteria.forClass(Cheese.class));
	}

	@Override
	public int saveCart(Cart cart) {
		return (Integer) getDAOCart().save(cart);
	}
	
	@Override
	public Cart findCartById(int id) {
		return getDAOCart().findById(id);
	}
	
	@Override
	public List<Cart> findAllCart() {
		return getDAOCart().findByCriteria(DetachedCriteria.forClass(Cart.class));
	}

}
