/**
 * 
 */
package com.anhtuan.manager.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.anhtuan.HibernateDaoTemplate;
import com.anhtuan.dao.UserDAO;
import com.anhtuan.dto.User;
import com.anhtuan.manager.UserManager;
import com.anhtuan.util.CommonUtils;

/**
 * @author anhtuan_truong
 *
 */
public class UserManagerImpl implements UserManager {
	
	private UserDAO userDAO;
	
	/**
	 * Injected by Spring
	 * @param userDAO
	 */
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@SuppressWarnings("unchecked")
	public HibernateDaoTemplate<User> getDAOUser() {
		return (HibernateDaoTemplate<User>) userDAO;
	}

	@Override
	public User findUser(String username) {
		return userDAO.findUserByUsername(username);
	}
	
	@Override
	public void updateUser(User user) {
		user.setPassword(CommonUtils.shax256(user.getPassword()));
		userDAO.updateUser(user); 
	}

	@Override
	public List<User> getAllUser() {
		return getDAOUser().findByCriteria(
				DetachedCriteria.forClass(User.class));
	}

}
