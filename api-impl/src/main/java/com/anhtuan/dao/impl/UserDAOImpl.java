/**
 * 
 */
package com.anhtuan.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.anhtuan.HibernateDaoTemplate;
import com.anhtuan.dao.UserDAO;
import com.anhtuan.dto.User;

/**
 * @author anhtuan_truong
 *
 */
public class UserDAOImpl extends HibernateDaoTemplate<User> implements UserDAO {

	public UserDAOImpl(Class classType) {
		super(classType);
	}

	@Override
	public User findUserByUsername(String username) {
		DetachedCriteria cri = DetachedCriteria.forClass(User.class);
		cri.add(Restrictions.eq("username", username));
		return super.findFirstByCriteria(cri);
	}
	
	@Override
	public void updateUser(User user) {
		super.update(user);
	}

}
