/**
 * 
 */
package com.anhtuan.dao.impl;

import com.anhtuan.HibernateDaoTemplate;
import com.anhtuan.dao.CartDAO;
import com.anhtuan.dto.Cart;

/**
 * @author anhtuan_truong
 *
 */
public class CartDAOImpl extends HibernateDaoTemplate<Cart> implements CartDAO {

	public CartDAOImpl(Class classType) {
		super(classType);
	}
	
	

}
