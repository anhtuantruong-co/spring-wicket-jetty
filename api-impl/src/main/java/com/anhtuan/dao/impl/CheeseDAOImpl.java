/**
 * 
 */
package com.anhtuan.dao.impl;

import com.anhtuan.HibernateDaoTemplate;
import com.anhtuan.dao.CheeseDAO;
import com.anhtuan.dto.Cheese;

/**
 * @author anhtuan_truong
 *
 */
public class CheeseDAOImpl extends HibernateDaoTemplate<Cheese> implements CheeseDAO{

	public CheeseDAOImpl(Class classType) {
		super(classType);
	}
	
	

}
