package com.anhtuan.util;
import com.anhtuan.dto.User;
import com.anhtuan.dto.UserStatus;

/**
 * 
 */

/**
 * @author anhtuan_truong
 *
 */
public class ObjectUtils {
	
	/*
	 * Get the last status of User
	 */
	public static UserStatus getLastStatusOfUser(User user) {
		for (UserStatus userStatus : user.getStatuses()) {
			if (userStatus.getValidTo() == null) {
				return userStatus;
			}
		}
		return null;
	}

}
