/**
 * 
 */
package com.anhtuan.exception;

/**
 * @author anhtuan_truong
 *
 */
public class AuthenticationException extends BusinessException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6273683002962335241L;
	
	private AuthenticationErrorType errorType;
	
	public enum AuthenticationErrorType {
		WRONG_AUTHENTICATION,
		LOCKED_ACCOUNT
	}

	public AuthenticationException() {
		super();
	}
	
	public AuthenticationException(AuthenticationErrorType errorType) {
		super();
		this.errorType = errorType;
	}
	
	public AuthenticationErrorType getErrorType() {
		return errorType;
	}

}
