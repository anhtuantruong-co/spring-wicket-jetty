/**
 * 
 */
package com.anhtuan;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.anhtuan.framework.hibernate.Auditable;

/**
 * @author anhtuan_truong
 * 
 */

/*
 * Using this annotation for children not having exception
 * org.hibernate.AnnotationException: No identifier specified for entity.
 * 
 * MappedSuperClass must be used to inherit properties, associations, and
 * methods. Entity inheritance must be used when you have an entity, and several
 * sub-entities.
 * 
 * @link
 * http://stackoverflow.com/questions/9667703/jpa-implementing-model-hiearchy
 * -mappedsuperclass-vs-inheritence
 */
@MappedSuperclass
public abstract class AbstractAnhTuanEntity implements Serializable, Auditable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int id;
	private Date creationDate;
	private Date modifiedDate;

	/*
	 * @GeneratedValue(generator = "cmsuuid")
	 * 
	 * @GenericGenerator(name = "cmsuuid", strategy =
	 * "com.fullsix.edito.util.OnDemandUUIDGenerator")
	 */
	@Id
	@GenericGenerator(name = "generator", strategy = "increment")
	@GeneratedValue(generator = "generator")
	@Column(length = 32)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creation_date", updatable = false, insertable = true)
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "modified_date", updatable = false, insertable = true)
	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Transient
	public boolean isPersisted() {
		return id > 0;
	}

}
