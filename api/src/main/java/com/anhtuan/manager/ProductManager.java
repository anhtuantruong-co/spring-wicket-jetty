package com.anhtuan.manager;

import java.util.List;

import com.anhtuan.dto.Cart;
import com.anhtuan.dto.Cheese;

public interface ProductManager {
	
	
	
	public List<Cheese> getAllCheeses();
	
	public int saveCart(Cart cart);
	
	public Cart findCartById(int id);
	
	public List<Cart> findAllCart();

}
