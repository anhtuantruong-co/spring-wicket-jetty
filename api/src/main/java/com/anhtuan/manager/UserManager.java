/**
 * 
 */
package com.anhtuan.manager;

import java.util.List;

import com.anhtuan.dto.User;

/**
 * @author anhtuan_truong
 *
 */
public interface UserManager {

	public User findUser(String username);
	
	public void updateUser(User user);

	public List<User> getAllUser();
}
