/**
 * 
 */
package com.anhtuan.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.anhtuan.AbstractAnhTuanEntity;
import com.anhtuan.type.RightType;

/**
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name = "T_RIGHT")
public class Right extends AbstractAnhTuanEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5008210849834226137L;

	private String name;
	private RightType type;

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 5)
	@Enumerated(EnumType.STRING)
	public RightType getType() {
		return type;
	}

	public void setType(RightType type) {
		this.type = type;
	}
}
