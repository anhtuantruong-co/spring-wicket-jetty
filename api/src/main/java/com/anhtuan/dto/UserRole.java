/**
 * 
 */
package com.anhtuan.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author anhtuan_truong
 *
 */
@Embeddable
//Class with Embeddable or (linked entity) must have at least 1 independent property
public class UserRole implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4283402532893062595L;
	
	
	private Date created;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created")
	public Date getCreated() {
		return created;
	}
	
	public void setCreated(Date created) {
		this.created = created;
	}
}
