/**
 * 
 */
package com.anhtuan.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.MapKeyManyToMany;

import com.anhtuan.AbstractAnhTuanEntity;

/**
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name = "T_SHOPPING_CART")
public class Cart extends AbstractAnhTuanEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Address billingAddress = new Address();
	private Map<Cheese, CartCheese> cartCheeses = new HashMap<Cheese, CartCheese>();

	private List<CartCheeseDTO> cheeses = new ArrayList<CartCheeseDTO>();

	// Make CascadeType=ALL to save entity Address when save Cart
	@OneToOne(mappedBy = "cart", targetEntity = Address.class, cascade = { CascadeType.ALL }, orphanRemoval = true)
	public Address getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Address other) {
		billingAddress = other;
	}

	public void calculate() {
		cheeses.clear();
		if (!cartCheeses.isEmpty()) {
			Set<Cheese> keySet = cartCheeses.keySet();
			for (Cheese cheese : keySet) {
				CartCheeseDTO dto = new CartCheeseDTO();
				dto.setCheese(cheese);
				dto.setQuantity(cartCheeses.get(cheese).getQuantity());
				dto.setUnit(cartCheeses.get(cheese).getUnit());
				cheeses.add(dto);
			}
		}
	}

	@Transient
	public double getTotal() {
		double total = 0;

		Set<Cheese> keySet = cartCheeses.keySet();

		for (Cheese cheese : keySet) {

			CartCheese cartCheese = cartCheeses.get(cheese);
			total += cartCheese.getUnit() * cartCheese.getQuantity();
		}

		return total;

	}

	public void add(Cheese cheese) {
		if (!cartCheeses.containsKey(cheese)) {
			CartCheese cartCheese = new CartCheese();
			cartCheese.setQuantity(1);
			cartCheese.setUnit(cheese.getPrice());
			cartCheeses.put(cheese, cartCheese);
		} else {
			cartCheeses.get(cheese).setQuantity(
					cartCheeses.get(cheese).getQuantity() + 1);
		}
		calculate();
	}

	public void remove(Cheese cheese) {
		cartCheeses.remove(cheese);
		calculate();
	}

	/**
	 * TODO:
	 * Hibernate org.hibernate.LazyInitializationException: failed to lazily initialize a collection of role.
	 * To prevent this, make eager fetch temporarily.
	 * @return
	 */
	@CollectionOfElements(targetElement = CartCheese.class, fetch = FetchType.EAGER)
	@JoinTable(name = "T_CART_CHEESE", joinColumns = { @JoinColumn(name = "cart", referencedColumnName = "id") })
	@MapKeyManyToMany(targetEntity = Cheese.class, joinColumns = { @JoinColumn(table = "T_CART_CHEESE", name = "cheese") })
	public Map<Cheese, CartCheese> getCartCheeses() {
		return cartCheeses;
	}

	public void setCartCheeses(Map<Cheese, CartCheese> cartCheeses) {
		this.cartCheeses = cartCheeses;
	}
}
