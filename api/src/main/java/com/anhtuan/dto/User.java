/**
 * 
 */
package com.anhtuan.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.MapKeyManyToMany;

import com.anhtuan.AbstractAnhTuanEntity;
import com.anhtuan.type.UserStatusType;
import com.anhtuan.util.ObjectUtils;

/**
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name="T_USER")
public class User extends AbstractAnhTuanEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String retypePassword;
	private String email;
	private int retryLogin;
	private Date lastLoginSuccessfull;
	private String name;
	private boolean active;
	
	private Set<UserStatus> statuses = new HashSet<UserStatus>();
	private Map<Role, UserRole> roles = new HashMap<Role, UserRole>();
	
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "username", nullable = false)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Transient
	public String getRetypePassword() {
		return retypePassword;
	}
	
	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
	
	@Column(name="email", nullable=false)
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="retry_login", nullable = false)
	public int getRetryLogin() {
		return retryLogin;
	}
	
	public void setRetryLogin(int retryLogin) {
		this.retryLogin = retryLogin;
	}

	@Transient
	public boolean isActive() {
		active = false;
		if (statuses != null) {
			for (UserStatus status : statuses) {
				if (status.getValidTo() == null) {
					active = UserStatusType.ACTIV == status.getStatus();
					break;
				}
			}
		}
		
		return active;
	}
	
	@OneToMany(mappedBy="owner", fetch=FetchType.EAGER, orphanRemoval=true, cascade=CascadeType.ALL)
	public Set<UserStatus> getStatuses() {
		return statuses;
	}
	
	public void setStatuses(Set<UserStatus> statuses) {
		this.statuses = statuses;
	}
	
	/**
	 * TODO:
	 * Hibernate org.hibernate.LazyInitializationException: failed to lazily initialize a collection of role.
	 * To prevent this, make eager fetch temporarily.
	 * @return
	 */
	@CollectionOfElements(targetElement = UserRole.class, fetch = FetchType.EAGER)
	@JoinTable(name = "T_USER_ROLE", joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") })
	@MapKeyManyToMany(targetEntity = Role.class, joinColumns = { @JoinColumn(table = "T_USER_ROLE", name = "role_id") })
	public Map<Role, UserRole> getRoles() {
		return roles;
	}
	
	public void setRoles(Map<Role, UserRole> roles) {
		this.roles = roles;
	}

	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name="last_login_successful")
	public Date getLastLoginSuccessfull() {
		return lastLoginSuccessfull;
	}
	
	public void setLastLoginSuccessfull(Date lastLoginSuccessfull) {
		this.lastLoginSuccessfull = lastLoginSuccessfull;
	}
	
	public void addStatus(UserStatusType type) {
		UserStatus lastStatusOfUser = ObjectUtils.getLastStatusOfUser(this);
		lastStatusOfUser.setValidTo(new Date());
		try {
			UserStatus newStatus = (UserStatus) lastStatusOfUser.clone();
			newStatus.setStatus(type);
			newStatus.setValidFrom(new Date());
			this.getStatuses().add(newStatus);
		} catch (CloneNotSupportedException e) {
			// Do nothing
		}
	}
}
