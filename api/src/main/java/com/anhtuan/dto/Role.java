/**
 * 
 */
package com.anhtuan.dto;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import org.hibernate.annotations.CollectionOfElements;
import org.hibernate.annotations.MapKeyManyToMany;

import com.anhtuan.AbstractAnhTuanEntity;

/**
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name = "T_ROLE")
public class Role extends AbstractAnhTuanEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2000852541072575814L;

	private String name;
	private Map<Right, RoleRight> rights = new HashMap<Right, RoleRight>();

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * TODO:
	 * Hibernate org.hibernate.LazyInitializationException: failed to lazily initialize a collection of role.
	 * To prevent this, make eager fetch temporarily.
	 * @return
	 */
	@CollectionOfElements(targetElement = RoleRight.class, fetch = FetchType.EAGER)
	@JoinTable(name = "T_ROLE_RIGHT", joinColumns = { @JoinColumn(name = "role_id", referencedColumnName = "id") })
	@MapKeyManyToMany(targetEntity = Right.class, joinColumns = { @JoinColumn(table = "T_ROLE_RIGHT", name = "right_Id") })
	
	public Map<Right, RoleRight> getRights() {
		return rights;
	}
	
	public void setRights(Map<Right, RoleRight> rights) {
		this.rights = rights;
	}
}
