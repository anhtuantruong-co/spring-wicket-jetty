/**
 * 
 */
package com.anhtuan.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author anhtuan_truong
 *
 */
@Embeddable
// Class with Embeddable or (linked entity) must have at least 1 independent property
public class CartCheese implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int quantity;
	private double unit;
	
	@Column(name="quantity", nullable=false)
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="unit", nullable=false)
	public double getUnit() {
		return unit;
	}
	
	public void setUnit(double unit) {
		this.unit = unit;
	}

}
