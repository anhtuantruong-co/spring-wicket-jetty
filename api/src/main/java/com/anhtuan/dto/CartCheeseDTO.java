/**
 * 
 */
package com.anhtuan.dto;

import java.io.Serializable;

/**
 * @author anhtuan_truong
 *
 */
public class CartCheeseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Cheese cheese;
	private int quantity;
	private double unit;
	

	public Cheese getCheese() {
		return cheese;
	}
	
	public void setCheese(Cheese cheese) {
		this.cheese = cheese;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public double getUnit() {
		return unit;
	}
	
	public void setUnit(double unit) {
		this.unit = unit;
	}
}
