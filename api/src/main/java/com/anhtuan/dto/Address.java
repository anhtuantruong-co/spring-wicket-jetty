package com.anhtuan.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.anhtuan.AbstractAnhTuanEntity;

@Entity
@Table(name="T_ADDRESS")
public class Address extends AbstractAnhTuanEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String street;
	private String zipcode;
	private String city;
	
	private Cart cart;
	
    @OneToOne(cascade={CascadeType.ALL}, targetEntity=Cart.class)
    @JoinColumn(name="cart", nullable =false, unique=true)
	public Cart getCart() {
		return cart;
	}
	
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	
	@Column(name="NAME", nullable=false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="STREET", nullable= false)
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	
	@Column(name="ZIP_CODE", length=4, nullable=false)
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	@Column(name="CITY", nullable=false)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	

}
