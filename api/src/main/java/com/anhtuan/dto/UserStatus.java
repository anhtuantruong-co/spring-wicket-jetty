/**
 * 
 */
package com.anhtuan.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.anhtuan.AbstractAnhTuanEntity;
import com.anhtuan.type.UserStatusType;

/**
 * @author anhtuan_truong
 * 
 */
@Entity
@Table(name = "T_USER_STATUS")
public class UserStatus extends AbstractAnhTuanEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7938863142736211544L;

	private User owner;
	private UserStatusType status;
	private Date validFrom;
	private Date validTo;

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "owner", nullable = false)
	public User getOwner() {
		return owner;
	}
	
	public void setOwner(User user) {
		this.owner = user;
	}
	
	@Column(name="status", nullable=false, length=5)
	@Enumerated(EnumType.STRING)
	public UserStatusType getStatus() {
		return status;
	}
	
	public void setStatus(UserStatusType status) {
		this.status = status;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="valid_from", nullable=false)
	public Date getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="valid_to")
	public Date getValidTo() {
		return validTo;
	}
	
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		UserStatus newStatus = new UserStatus();
		newStatus.setOwner(owner);
		return newStatus;
	}

}
