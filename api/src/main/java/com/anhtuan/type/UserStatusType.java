package com.anhtuan.type;

public enum UserStatusType {
	
	ACTIV("ACTIV"),
	DEACT("DEACT"),
	LOCKE("LOCKE");
	
	private String code;
	
	private UserStatusType(String code) {
		this.code = code;
	}
	
	public UserStatusType getTypeFromCode(String code) {
		for (UserStatusType item : UserStatusType.values()) {
			if (item.code.equals(code)) {
				return item;
			}
		}
		
		// TODO: throws exception that enum type is not supported
		return null;
	}
}
