/**
 * 
 */
package com.anhtuan.type;

/**
 * @author anhtuan_truong
 *
 */
public enum RightType {
	
	EDITA("EDIT"),
	DELETE("DELET");
	
	private String code;
	
	private RightType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}

}
