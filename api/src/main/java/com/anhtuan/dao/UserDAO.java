/**
 * 
 */
package com.anhtuan.dao;

import java.util.List;

import com.anhtuan.dto.User;

/**
 * @author anhtuan_truong
 *
 */
public interface UserDAO {
	
	public User findUserByUsername(String username);
	
	public void updateUser(User user);

}
