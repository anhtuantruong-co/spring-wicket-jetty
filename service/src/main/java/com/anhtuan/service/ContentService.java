/**
 * 
 */
package com.anhtuan.service;

import java.util.List;

import com.anhtuan.dto.Cart;
import com.anhtuan.dto.Cheese;

/**
 * @author anhtuan_truong
 *
 */
public interface ContentService {
	
	public List<Cheese> getAllCheeses();
	
	public int saveCart(Cart cart);
	
	public Cart findCartById(int id);
	
	public List<Cart> findAllCart();

}
