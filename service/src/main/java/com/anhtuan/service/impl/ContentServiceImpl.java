/**
 * 
 */
package com.anhtuan.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.anhtuan.dto.Cart;
import com.anhtuan.dto.Cheese;
import com.anhtuan.manager.ProductManager;
import com.anhtuan.service.ContentService;

/**
 * @author anhtuan_truong
 *
 */
@Service
public class ContentServiceImpl implements ContentService {
	
	
	private ProductManager productManager;
	
	/**
	 * Injected by Spring
	 * @param productManager
	 */
	public void setProductManager(ProductManager productManager) {
		this.productManager = productManager;
	}

	@Override
	public List<Cheese> getAllCheeses() {
		return productManager.getAllCheeses();
	}
	
	@Override
	public int saveCart(Cart cart) {
		return productManager.saveCart(cart);
	}
	
	@Override
	public Cart findCartById(int id) {
		return productManager.findCartById(id);
	}
	
	@Override
	public List<Cart> findAllCart() {
		return productManager.findAllCart();
	}

}
