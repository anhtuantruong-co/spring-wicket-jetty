/**
 * 
 */
package com.anhtuan.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anhtuan.dto.User;
import com.anhtuan.dto.UserStatus;
import com.anhtuan.exception.AuthenticationException;
import com.anhtuan.exception.AuthenticationException.AuthenticationErrorType;
import com.anhtuan.manager.UserManager;
import com.anhtuan.service.UserService;
import com.anhtuan.type.UserStatusType;
import com.anhtuan.util.CommonUtils;
import com.anhtuan.util.Constants;
import com.anhtuan.util.ObjectUtils;

/**
 * @author anhtuan_truong
 * 
 */
public class UserServiceImpl implements UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

	private UserManager userManager;

	/**
	 * Injected by Spring
	 * 
	 * @param userManager
	 */
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	@Override
	public User doLogin(String username, String password)
			throws AuthenticationException {
		User user = userManager.findUser(username);

		if (user != null
				&& user.getPassword().equals(CommonUtils.shax256(password))) {
			UserStatus lastStatus = ObjectUtils.getLastStatusOfUser(user);
			switch (lastStatus.getStatus()) {
			case ACTIV:
				LOG.info("Login successfully");
				user.setRetryLogin(0);
				user.setLastLoginSuccessfull(new Date());
				// TODO: set to user context
				return user;
			case LOCKE:
				throw new AuthenticationException(
						AuthenticationErrorType.LOCKED_ACCOUNT);
			default:
				break;
			}
		} else if (user != null) {
			// Wrong Password
			user.setRetryLogin(user.getRetryLogin() + 1);
			if (Constants.MAX_RETRY == user.getRetryLogin()) {
				user.addStatus(UserStatusType.LOCKE);
				userManager.updateUser(user);
				LOG.warn("Wrong username or password");
				throw new AuthenticationException(
						AuthenticationErrorType.LOCKED_ACCOUNT);
			} else {
				userManager.updateUser(user);
				throw new AuthenticationException(
						AuthenticationErrorType.WRONG_AUTHENTICATION);
			}
		}

		throw new AuthenticationException(
				AuthenticationErrorType.WRONG_AUTHENTICATION);
	}

	@Override
	public List<User> getAllUser() {
		return userManager.getAllUser();
	}
	
	@Override
	public void updateUser(User user) {
		userManager.updateUser(user);
	}

}
