/**
 * 
 */
package com.anhtuan.service;

import java.util.List;

import com.anhtuan.dto.User;
import com.anhtuan.exception.AuthenticationException;

/**
 * @author anhtuan_truong
 *
 */
public interface UserService {
	
	public User doLogin(String username, String password) throws AuthenticationException;

	public List<User> getAllUser();
	
	public void updateUser(User user);

}
