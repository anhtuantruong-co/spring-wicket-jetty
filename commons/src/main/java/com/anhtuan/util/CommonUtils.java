/**
 * 
 */
package com.anhtuan.util;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * @author anhtuan_truong
 *
 */
public class CommonUtils {

	public static String shax256(String password) {
		return DigestUtils.shaHex(password);
	}
}
