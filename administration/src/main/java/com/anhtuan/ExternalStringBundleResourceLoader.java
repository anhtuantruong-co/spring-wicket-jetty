/**
 * 
 */
package com.anhtuan;

import java.util.Locale;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.Session;
import org.apache.wicket.core.util.resource.locator.ResourceNameIterator;
import org.apache.wicket.resource.IPropertiesFactory;
import org.apache.wicket.resource.Properties;
import org.apache.wicket.resource.loader.IStringResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anhtuan_truong
 *
 */
public class ExternalStringBundleResourceLoader implements
		IStringResourceLoader {

	private static final Logger logger = LoggerFactory.getLogger(ExternalStringBundleResourceLoader.class);

    private final String bundleName;

    public ExternalStringBundleResourceLoader(String bundleName) {
        this.bundleName = bundleName;
    }

    public final String loadStringResource(final Class<?> clazz, final String key, Locale locale, final String style, final String variation) {
        if (locale == null) {
            locale = Session.exists() ? Session.get().getLocale() : Locale.getDefault();
        }
        logger.debug("clazz: {}, key: {}, locale: {}, style: {}, variation: {}",
                new Object[]{
                        clazz, key, locale, style, variation
                });

        ResourceNameIterator iter = newResourceNameIterator(bundleName, locale, style, variation);

        while (iter.hasNext()) {
            String path = iter.next();
            IPropertiesFactory propertiesFactory = getPropertiesFactory();
            Properties props = propertiesFactory.load(clazz, path);
            if (props != null) {
                String value = props.getString(key);
                if (value != null) {
                    return value;
                }
            }
        }
        return null;

    }

    public final String loadStringResource(final Component component, final String key, Locale locale, final String style,
            final String variation) {
        if(component == null){
            // this does happen when using ResourceModel
            return loadStringResource(CheesrApplication.class, key, locale, style, variation);
        }
        return loadStringResource(component.getClass(), key, locale, style, variation);
    }

    protected IPropertiesFactory getPropertiesFactory() {
        return Application.get().getResourceSettings().getPropertiesFactory();
    }

    protected ResourceNameIterator newResourceNameIterator(final String path, final Locale locale, final String style,
            final String variation) {
        return (ResourceNameIterator) Application.get().getResourceSettings().getResourceStreamLocator().newResourceNameIterator(path, locale, style, variation, null, false);
    }

}
