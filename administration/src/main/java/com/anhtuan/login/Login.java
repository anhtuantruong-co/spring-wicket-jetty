/**
 * 
 */
package com.anhtuan.login;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;

import com.anhtuan.CheesrSession;
import com.anhtuan.HomePage;
import com.anhtuan.administration.ManageUser;
import com.anhtuan.dto.Address;
import com.anhtuan.dto.User;
import com.anhtuan.exception.AuthenticationException;
import com.anhtuan.exception.AuthenticationException.AuthenticationErrorType;
import com.anhtuan.service.UserService;

/**
 * @author anhtuan_truong
 * 
 */
@MountPath("/login")
public class Login extends WebPage {
	
	@SpringBean(name="userService")
	private UserService userService;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6585604312608997331L;

	public Login() {
		
		// Adding feedback panel to display error message when validation on
		// form is fail
		add(new FeedbackPanel("feedback"));

		// Adding form component for user type data
		Form<Address> form = new Form<Address>("form");
		add(form);
		User user = ((CheesrSession) getSession()).getUser();
		form.add(new TextField<User>("username", new PropertyModel<User>(user,
				"username")).setRequired(true));
		// We do not specify the type of password field in here.
		form.add(new PasswordTextField("password", new PropertyModel<String>(user,
				"password")).setRequired(true));

		// Button cancel
		form.add(new Link("reset") {
			@Override
			public void onClick() {
				setResponsePage(HomePage.class);
			}
		});

		// Button order
		form.add(new Button("login", new ResourceModel("login.submit")) {
			@Override
			public void onSubmit() {
				User user = ((CheesrSession) getSession()).getUser();
				User userLogin;
				try {
					userLogin = userService.doLogin(user.getUsername(), user.getPassword());
					setResponsePage(ManageUser.class);
//					setResponsePage(ViewBill.class);
				} catch (AuthenticationException e) {
					if (AuthenticationErrorType.WRONG_AUTHENTICATION == e.getErrorType()) {
						// Using the error feedback from wicket
						error(getString("wrong.authentication"));
					} else if (AuthenticationErrorType.LOCKED_ACCOUNT == e.getErrorType()) {
						error(getString("account.locked"));
					}
				}
			}
		});
	}
	

}
