/**
 * 
 */
package com.anhtuan;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.core.util.resource.UrlResourceStream;
import org.apache.wicket.core.util.resource.locator.ResourceStreamLocator;
import org.apache.wicket.util.resource.IResourceStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author anhtuan_truong
 * 
 */
public class WebappResourceStreamLocator extends ResourceStreamLocator {
	private static final Logger LOG = LoggerFactory
			.getLogger(WebappResourceStreamLocator.class);

	private final String toStrip;
	private List<String> alternateStrips;

	public WebappResourceStreamLocator(Package pack) {
		toStrip = pack.getName().replaceAll("\\.", "/") + "/";
	}

	@Override
	public IResourceStream locate(Class<?> clazz, String path) {
		IResourceStream located = super.locate(clazz,
				trimFolders(path, toStrip));
		if (located == null) {
			if (alternateStrips != null) {
				for (String alternative : alternateStrips) {
					located = super.locate(clazz,
							trimFolders(path, alternative));
					if (located != null) {
						break;
					}
				}
			}
			if (located == null) {
				located = super.locate(clazz, path);
			}
		}
		if (located == null && StringUtils.startsWith(path, "/WEB-INF/")) {
			try {
				URL url = CheesrApplication.get().getServletContext().getResource(path);
				if (url != null) {
					located = new UrlResourceStream(url);
				}
			} catch (MalformedURLException e) {
				LOG.trace("Unable to load resource by URL", e);
			}
		}
		return located;
	}

	/**
	 * Adds an alternative to the toStrip path. Allows path stripping in
	 * overlayed webapps.
	 * 
	 * @param altPath
	 *            the alternative toStrip path
	 */
	public void addAlternatePathToStrip(String altPath) {
		if (alternateStrips == null) {
			alternateStrips = new ArrayList<String>();
		}
		alternateStrips.add(altPath);
	}

	public void addAlternatePackage(Package pack) {
		String path = pack.getName().replaceAll("\\.", "/") + "/";
		addAlternatePathToStrip(path);
	}

	/**
	 * Remove package name from path.
	 * 
	 * @param path
	 *            The path to trim
	 * @return The trimmed path
	 */
	private String trimFolders(String path, String toStripPath) {
		return path.replace(toStripPath, "");
	}
}
