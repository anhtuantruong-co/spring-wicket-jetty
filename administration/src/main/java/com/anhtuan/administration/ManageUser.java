/**
 * 
 */
package com.anhtuan.administration;

import java.util.List;

import org.apache.wicket.event.IEvent;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;

import com.anhtuan.dto.User;
import com.anhtuan.service.UserService;

/**
 * @author anhtuan_truong
 *
 */
@MountPath("/manageUser")
public class ManageUser extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 889788266835631354L;
	
	@SpringBean(name="userService")
	private UserService userService;
	
	private ChoiceRenderer<User> userRender = new ChoiceRenderer<User>("name");
	private Model<User> userModel = new Model<User>();
	
	public ManageUser() {
		super();
		add(new DropDownChoice<User>("users", userModel, loadUser(), userRender) {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1763980618374349260L;

			@Override
			protected boolean wantOnSelectionChangedNotifications() {
				return true;
			}
		});
		add(new UserDetailPanel("userDetail", userModel));
	}
	
	private List<User> loadUser() {
		return userService.getAllUser();
	}
	
	@Override
	public void onEvent(IEvent<?> event) {
		if (event.getPayload() instanceof ReloadUsers) {
			// TODO:
		}
		super.onEvent(event);
	}

}
