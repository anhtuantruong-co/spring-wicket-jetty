/**
 * 
 */
package com.anhtuan.administration;

import org.apache.wicket.event.Broadcast;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.anhtuan.HomePage;
import com.anhtuan.dto.User;
import com.anhtuan.service.UserService;

/**
 * @author anhtuan_truong
 * 
 */
public class UserDetailPanel extends Panel {
	
	
	@SpringBean(name="userService")
	private UserService userService;
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 5541040196925285910L;
	Form<User> form;

	
	@SuppressWarnings("rawtypes")
	public UserDetailPanel(String id, Model<User> userModel) {
		super(id);
		// Adding feedback panel to display error message when validation on
		// form is fail
		add(new FeedbackPanel("feedback"));

		// Adding form component for user type data
		form = new Form<User>("form",
				new CompoundPropertyModel<User>(userModel));
		add(form);
		form.add(new TextField<User>("name").setRequired(true));
		form.add((new TextField<User>("username")).setEnabled(false));
		PasswordTextField passwordTextField = new PasswordTextField("password");
		form.add(passwordTextField.setRequired(true));
		PasswordTextField retypePassword = new PasswordTextField("retypePassword");
		form.add(retypePassword);
		form.add(new TextField<User>("email").setRequired(true));
		
		// Adding password equal validator
		form.add(new EqualPasswordInputValidator(passwordTextField, retypePassword));
		
		form.add(new CheckBox("active").setEnabled(false));
		

		// Button order
		form.add(new Button("login") {
			/**
			 * 
			 */
			private static final long serialVersionUID = -7952276353249811364L;

			@Override
			public void onSubmit() {
				User user = form.getModelObject();
				userService.updateUser(user);
				send(getPage(), Broadcast.BREADTH, new ReloadUsers(null));
			}
		});

		// Button cancel
		form.add(new Link("goHome") {
			/**
			 * 
			 */
			private static final long serialVersionUID = -8134513368498886682L;

			@Override
			public void onClick() {
				setResponsePage(HomePage.class);
			}
		});

	}
}
