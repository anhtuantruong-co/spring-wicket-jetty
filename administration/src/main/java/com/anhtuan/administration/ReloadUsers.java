/**
 * 
 */
package com.anhtuan.administration;

import org.apache.wicket.ajax.AjaxRequestTarget;

/**
 * @author anhtuan_truong
 *
 */
public class ReloadUsers {
	
	private final AjaxRequestTarget target;

    public ReloadUsers(AjaxRequestTarget target) {
        this.target = target;
    }

    public AjaxRequestTarget getTarget() {
        return target;
    }

}
