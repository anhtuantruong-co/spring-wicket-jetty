/**
 * 
 */
package com.anhtuan.bill;

import java.util.List;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.wicketstuff.annotation.mount.MountPath;

import com.anhtuan.dto.Cart;
import com.anhtuan.service.ContentService;

/**
 * @author anhtuan_truong
 *
 */
@MountPath("/viewbill")
public class ViewBill extends WebPage {
	
	
	@SpringBean(name="contentService")
	ContentService contentService;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ViewBill() {
		PageableListView<Cart> carts = new PageableListView<Cart>("carts", new ViewBillModel(), 5) {
			
			@Override
			protected void populateItem(ListItem<Cart> item) {
				item.add(new ViewBillRow(item.getModel()));
			}
		};
		add(carts);
	}
	
	
	private class ViewBillModel extends LoadableDetachableModel<List<Cart>>{

		@Override
		protected List<Cart> load() {
			List<Cart> findAllCart = contentService.findAllCart();
			return findAllCart;
		}
	}
	
	private static class ViewBillRow extends WebMarkupContainer {

		private static final long serialVersionUID = 1L;

		public ViewBillRow(IModel<Cart> version) {
			super("item", new CompoundPropertyModel<Cart>(version));
//			PageParameters params = new PageParameters().add("cartid", version.getObject().getId());
			// TODO: 
//			add(new BookmarkablePageLink<Void>("link", NodeViewPage.class, params).add(new Label("node.absolutePath")));
			add(new Label("cartid", version.getObject().getId()));
			add(new Label("cost", version.getObject().getTotal()));
			add(new Label("creationDate", version.getObject().getCreationDate()));
		}
	}

}
