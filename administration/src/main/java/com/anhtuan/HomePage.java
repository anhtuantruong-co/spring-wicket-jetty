/**
 * 
 */
package com.anhtuan;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

import com.anhtuan.login.Login;
import com.anhtuan.shopping.Order;

/**
 * @author anhtuan_truong
 * 
 */
public class HomePage extends WebPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8084066751515095858L;

	public HomePage() {
		super();

		add(new BookmarkablePageLink<Void>("goShop", Order.class));
		add(new BookmarkablePageLink<Void>("login", Login.class));
	}

}
