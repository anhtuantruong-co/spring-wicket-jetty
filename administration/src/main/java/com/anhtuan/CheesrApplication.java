/**
 * 
 */
package com.anhtuan;

import java.util.List;

import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.core.util.file.WebApplicationPath;
import org.apache.wicket.core.util.resource.locator.caching.CachingResourceStreamLocator;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.resource.loader.IStringResourceLoader;
import org.apache.wicket.settings.IResourceSettings;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.apache.wicket.util.lang.PackageName;
import org.wicketstuff.annotation.scan.AnnotatedMountScanner;

/**
 * @author anhtuan_truong
 * 
 */
public class CheesrApplication extends WebApplication {

	/**
	 * Constructor
	 */
	public CheesrApplication() {
	}

	@Override
	protected void init() {
		configureResourceSettings();
		mountPaths();
		// This line code below using bean in Spring.
		// This will make all annotation @SpingBean work properly.
		getComponentInstantiationListeners().add(
				new SpringComponentInjector(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	@Override
	public Session newSession(Request request, Response response) {
		return new CheesrSession(CheesrApplication.this, request);
	}

	protected void configureResourceSettings() {
		// Configure mark-up resource
		// must have the same package
		// Ex: Index.java is in com.anhtuan
		// Index.html must have is in com.anhtuan
		getResourceSettings().getResourceFinders().add(
				new WebApplicationPath(getServletContext(), "wicket"));

		// Configure resource bundle: the package of properties file and java class must have the same
		// like when configuring mark-up resource
		getResourceSettings().setResourceStreamLocator(
				new CachingResourceStreamLocator(
						new WebappResourceStreamLocator(CheesrApplication.class
								.getPackage())));
		registerResourceLoaders(getResourceSettings());
	}
	
	private void mountPaths() {
		// Add this line code below to make @MountPath of webPage in wicket work such as Login.java
		new AnnotatedMountScanner().scanPackage(PackageName.forClass(CheesrApplication.class).getName()).mount(this);
	}

	protected void registerResourceLoaders(IResourceSettings resourceSettings) {
		// load old CMS resource bundle, in addition to wicket bundles
		List<IStringResourceLoader> resourceLoaders = resourceSettings
				.getStringResourceLoaders();
		resourceLoaders
				.add(new ExternalStringBundleResourceLoader("/Resources"));
		/*resourceLoaders
				.add(new ExternalStringBundleResourceLoader("/Templates"));
		resourceLoaders.add(new ExternalStringBundleResourceLoader(
				"/WEB-INF/Templates"));*/
	}
}
