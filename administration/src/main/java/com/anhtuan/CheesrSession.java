/**
 * 
 */
package com.anhtuan;

import org.apache.wicket.Application;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

import com.anhtuan.dto.Cart;
import com.anhtuan.dto.User;

/**
 * @author anhtuan_truong
 * 
 */
public class CheesrSession extends WebSession {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3488454707425042219L;
	private Cart cart = new Cart();
	private User user = new User();

	protected CheesrSession(Application application, Request request) {
		super(request);
	}

	public Cart getCart() {
		return cart;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
}
