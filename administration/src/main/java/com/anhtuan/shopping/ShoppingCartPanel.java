/**
 * 
 */
package com.anhtuan.shopping;

import java.io.Serializable;
import java.text.NumberFormat;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

import com.anhtuan.dto.Cart;
import com.anhtuan.dto.CartCheeseDTO;

/**
 * @author anhtuan_truong
 * 
 */
public class ShoppingCartPanel extends Panel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5117491155012894064L;
	private Cart cart;

	public ShoppingCartPanel(String id, Cart cart) {
		super(id);
		this.cart = cart;

		add(new ListView("cart", new PropertyModel(this, "cart.cheeses")) {
			@Override
			protected void populateItem(ListItem item) {
				final CartCheeseDTO cheese = (CartCheeseDTO) item.getModelObject();
				item.add(new Label("name", cheese.getCheese().getName()));
				item.add(new Label("price", "$" + cheese.getUnit()));
				item.add(new Label("quantity", cheese.getQuantity()));
//				item.add(removeLink("remove", item));
				item.add(new Link("remove") {

					@Override
					public void onClick() {
						getCart().remove(cheese.getCheese());
						
					}
					
				});
			}
		});
		add(new Label("total", new Model() {
			@Override
			// @ATR: Why leave this type is Serializable
			public Serializable getObject() {
				NumberFormat nf = NumberFormat.getCurrencyInstance();
				return nf.format(getCart().getTotal());
			}
		}));
	}

	private Cart getCart() {
		return cart;
	}

}
