/**
 * 
 */
package com.anhtuan.shopping;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.wicketstuff.annotation.mount.MountPath;

import com.anhtuan.dto.Cheese;

/**
 * @author anhtuan_truong
 * 
 */
@MountPath("/order")
public class Order extends CheesrPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	public Order() {
		PageableListView<Cheese> cheeses = new PageableListView<Cheese>("cheeses", getCheeses(), 5	) {
			@SuppressWarnings("rawtypes")
			@Override
			protected void populateItem(ListItem<Cheese> item) {
				Cheese cheese = item.getModelObject();
				item.add(new Label("name", cheese.getName()));
				item.add(new Label("description", cheese.getDescription()));
				item.add(new Label("price", "$" + cheese.getPrice()));

				// Add link
				item.add(new Link("add", item.getModel()) {
					@Override
					public void onClick() {
						Cheese selected = (Cheese) getModelObject();
						getCart().add(selected);
					}
				});
			}
		};
		add(cheeses);
		// Define page navigator on cheeses
		add(new PagingNavigator("navigator", cheeses));
		
		// adding Shopping cart
		add(new ShoppingCartPanel("shoppingcart", getCart()));

		add(new Link("checkout") {
			@Override
			public void onClick() {
				setResponsePage(new Checkout());
			}

			@Override
			public boolean isVisible() {
				return !getCart().getCartCheeses().isEmpty();
			}
		});
	}
}
