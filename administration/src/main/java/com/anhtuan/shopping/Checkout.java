/**
 * 
 */
package com.anhtuan.shopping;

import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.validation.validator.StringValidator;
import org.wicketstuff.annotation.mount.MountPath;

import com.anhtuan.dto.Address;
import com.anhtuan.dto.Cart;

/**
 * @author anhtuan_truong
 * 
 */
@MountPath("/checkout")
public class Checkout extends CheesrPage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1844556113737311059L;

	public Checkout() {

		// Adding feedback panel to display error message when validation on form is fail
		add(new FeedbackPanel("feedback"));
		
		// Adding form component for user type data
		Form<Address> form = new Form<Address>("form");
		add(form);
		Address address = getCart().getBillingAddress();
		TextField<Address> nameTxt = new TextField<Address>("name", new PropertyModel<Address>(address, "name"));
		form.add(nameTxt.setRequired(true).add(StringValidator.lengthBetween(5, 32)));
		form.add(new TextField<Address>("street", new PropertyModel<Address>(address, "street")).setRequired(true));
		form.add(new TextField<Address>("zipcode", new PropertyModel<Address>(address, "zipcode")).setRequired(true));
		form.add(new TextField<Address>("city", new PropertyModel<Address>(address, "city")).setRequired(true));

		// Button cancel
		form.add(new Link("cancel") {
			@Override
			public void onClick() {
				setResponsePage(Order.class);
			}
		});

		// Button order
		form.add(new Button("order") {
			@Override
			public void onSubmit() {
				Cart cart = getCart();
				cart.getBillingAddress().setCart(cart);
				int cartID = makeOrder(cart); 
				
				
				Cart newDTO = getCart(cartID);
				// charge customers’ credit card
				// ship cheeses to our customer
				// clean out shopping cart
				cart.getCartCheeses().clear();
				// return to front page
				setResponsePage(Order.class);
			}
		});
		
		add(new ShoppingCartPanel("shoppingcart", getCart()));
	}
}
