/**
 * 
 */
package com.anhtuan.shopping;

import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.anhtuan.CheesrSession;
import com.anhtuan.dto.Cart;
import com.anhtuan.dto.Cheese;
import com.anhtuan.service.ContentService;

/**
 * @author anhtuan_truong
 * 
 */
public abstract class CheesrPage extends WebPage {
	
	@SpringBean(name="contentService")
	private ContentService contentService;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CheesrSession getCheesrSession() {
		return (CheesrSession) getSession();
	}

	public Cart getCart() {
		return getCheesrSession().getCart();
	}

	public List<Cheese> getCheeses() {
		return contentService.getAllCheeses();
	}
	
	public int makeOrder(Cart cart) {
		return contentService.saveCart(cart); 
	}
	
	public Cart getCart(int id) {
		return contentService.findCartById(id); 
	}
	

}
