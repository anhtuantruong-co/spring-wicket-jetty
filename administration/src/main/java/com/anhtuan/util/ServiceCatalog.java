/**
 * 
 */
package com.anhtuan.util;

import org.apache.wicket.spring.injection.annot.SpringBean;

import com.anhtuan.service.ContentService;

/**
 * @author anhtuan_truong
 *
 */
public class ServiceCatalog {
	
	@SpringBean(name="contentService", required=true)
	private static ContentService contentService;
	
	public static ContentService getContentService() {
		return contentService;
	}

}
