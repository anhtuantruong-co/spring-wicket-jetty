<?xml version="1.0" encoding="UTF-8"?>
<!-- Licensed to the Apache Software Foundation (ASF) under one or more contributor 
	license agreements. See the NOTICE file distributed with this work for additional 
	information regarding copyright ownership. The ASF licenses this file to 
	You under the Apache License, Version 2.0 (the "License"); you may not use 
	this file except in compliance with the License. You may obtain a copy of 
	the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required 
	by applicable law or agreed to in writing, software distributed under the 
	License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS 
	OF ANY KIND, either express or implied. See the License for the specific 
	language governing permissions and limitations under the License. -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<artifactId>apache-tutorial-parent</artifactId>
		<groupId>com.anhtuan</groupId>
		<version>1.0-SNAPSHOT</version>
		<relativePath>../pom.xml</relativePath>
	</parent>
	<groupId>com.anhtuan</groupId>
	<artifactId>administration</artifactId>
	<packaging>war</packaging>
	<version>1.0-SNAPSHOT</version>
	<name>Administration</name>
	<!-- <organization> <name>company name</name> <url>company url</url> </organization> -->
	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>
	<properties>
		<wicket.version>6.12.0</wicket.version>
		<jetty.version>7.6.13.v20130916</jetty.version>
		<project.version>1.0-SNAPSHOT</project.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>
	<profiles>
		<!-- Profile initialization for H2 database -->
		<profile>
			<id>h2</id>
			<properties>
				<db.type>h2</db.type>
				<db.driver>org.h2.jdbcx.JdbcDataSource</db.driver>
				<db.url>jdbc:h2:./target/h2/anhtuan</db.url>
				<db.username>admin</db.username>
				<db.password>admin</db.password>
			</properties>
		</profile>
	</profiles>

	<dependencies>
		<!-- Project API -->
		<dependency>
			<groupId>com.anhtuan</groupId>
			<artifactId>api</artifactId>
			<version>${project.version}</version>
		</dependency>


		<!-- Project service -->
		<dependency>
			<groupId>com.anhtuan</groupId>
			<artifactId>service</artifactId>
			<version>${project.version}</version>
		</dependency>

		<!-- Contains org.springframework.web.context.ContextLoaderListener which 
			is configured in web.xml -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
			<version>3.1.3.RELEASE</version>
		</dependency>

		<!-- Contains org.apache.commons.dbcp.BasicDataSource -->
		<dependency>
			<groupId>commons-dbcp</groupId>
			<artifactId>commons-dbcp</artifactId>
			<version>1.4</version>
		</dependency>

		<!-- WICKET DEPENDENCIES -->
		<dependency>
			<groupId>org.apache.wicket</groupId>
			<artifactId>wicket-core</artifactId>
			<version>${wicket.version}</version>
		</dependency>

		<!-- Contain @SpringBean -->
		<dependency>
			<groupId>org.apache.wicket</groupId>
			<artifactId>wicket-spring</artifactId>
			<version>${wicket.version}</version>
		</dependency>
		
		<!-- Ex: @MountPath -->
		<dependency>
			<groupId>org.wicketstuff</groupId>
			<artifactId>wicketstuff-annotation</artifactId>
			<version>${wicket.version}</version>
		</dependency>

		<!-- OPTIONAL DEPENDENCY <dependency> <groupId>org.apache.wicket</groupId> 
			<artifactId>wicket-extensions</artifactId> <version>${wicket.version}</version> 
			</dependency> -->

		<!-- LOGGING DEPENDENCIES - LOG4J -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.6.4</version>
		</dependency>
		<dependency>
			<groupId>log4j</groupId>
			<artifactId>log4j</artifactId>
			<version>1.2.16</version>
		</dependency>

		<!-- JUNIT DEPENDENCY FOR TESTING -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.11</version>
			<scope>test</scope>
		</dependency>

		<!-- StringUtils in commons.lang -->
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>2.6</version>
		</dependency>

		
		<!-- JETTY DEPENDENCIES FOR TESTING -->
		<dependency>
			<groupId>org.eclipse.jetty.aggregate</groupId>
			<artifactId>jetty-all-server</artifactId>
			<version>${jetty.version}</version>
			<scope>provided</scope>
		</dependency>
	</dependencies>
	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
			</resource>
			<resource>
				<directory>src/main/java</directory>
				<includes>
					<include>**</include>
				</includes>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
			</resource>
		</resources>
		<testResources>
			<testResource>
				<directory>src/test/resources</directory>
			</testResource>
			<testResource>
				<directory>src/test/java</directory>
				<includes>
					<include>**</include>
				</includes>
				<excludes>
					<exclude>**/*.java</exclude>
				</excludes>
			</testResource>
		</testResources>
		<plugins>
			<plugin>
				<inherited>true</inherited>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>2.5.1</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
					<encoding>UTF-8</encoding>
					<showWarnings>true</showWarnings>
					<showDeprecation>true</showDeprecation>
				</configuration>
			</plugin>

			<!-- Configuration for Jetty -->
			<plugin>
				<!-- contains org.eclipse.jetty.webapp.WebAppContext in jetty-env -->
				<groupId>org.mortbay.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>${jetty.version}</version>
				<dependencies>
					<dependency>
						<groupId>com.h2database</groupId>
						<artifactId>h2</artifactId>
						<version>1.3.162</version>
					</dependency>
				</dependencies>
				<configuration>
					<!-- Theses system properties are defined in jetty-env.xml -->
					<systemProperties>
						<systemProperty>
							<name>dbdriver</name>
							<value>${db.driver}</value>
						</systemProperty>
						<systemProperty>
							<name>dburl</name>
							<value>${db.url}</value>
						</systemProperty>
						<systemProperty>
							<name>dbuser</name>
							<value>${db.username}</value>
						</systemProperty>
						<systemProperty>
							<name>dbpassword</name>
							<value>${db.password}</value>
						</systemProperty>
					</systemProperties>
					<connectors>
						<connector implementation="org.eclipse.jetty.server.nio.SelectChannelConnector">
							<port>8090</port>
							<maxIdleTime>3600000</maxIdleTime>
						</connector>
						<connector implementation="org.eclipse.jetty.server.ssl.SslSocketConnector">
							<port>8443</port>
							<maxIdleTime>3600000</maxIdleTime>
							<keystore>${project.build.directory}/test-classes/keystore</keystore>
							<password>wicket</password>
							<keyPassword>wicket</keyPassword>
						</connector>
					</connectors>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<downloadSources>true</downloadSources>
				</configuration>
			</plugin>
		</plugins>
	</build>


	<repositories>
		<repository>
			<id>Apache Nexus</id>
			<url>https://repository.apache.org/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
			</snapshots>
		</repository>
	</repositories>
</project>
