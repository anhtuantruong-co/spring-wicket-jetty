#!/bin/sh
#
# invoke with the path of a H2 script file as a parameter to load. If no parameter supplied, the default bootstrap-h2.sql
# is loaded

SQL="src/main/sql/bootstrap-h2.sql"
if [ ! -z "$1" ]; then
    SQL=$1
fi

CP=$HOME/.m2/repository/com/h2database/h2/1.3.162/h2-1.3.162.jar
TARGET=../administration/target/h2/anhtuan

echo "Installing $SQL in $TARGET"
java -cp "$CP" org.h2.tools.RunScript -url "jdbc:h2:$TARGET" -user "admin" -password "admin" -script $SQL
